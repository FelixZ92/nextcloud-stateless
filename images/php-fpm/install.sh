#!/usr/bin/env sh

NEXTCLOUD_VERSION=20.0.4

temp=$(mktemp -d)

cd "$temp" || exit 1
# download binaries
wget https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2

# verify checksum
wget https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.sha256
sha256sum -c nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.sha256 < nextcloud-${NEXTCLOUD_VERSION}.tar.bz2

# verify gpg
wget https://nextcloud.com/nextcloud.asc
wget https://download.nextcloud.com/server/releases/nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.asc

gpg --import nextcloud.asc
gpg --verify nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.asc nextcloud-${NEXTCLOUD_VERSION}.tar.bz2

tar -xjf nextcloud-${NEXTCLOUD_VERSION}.tar.bz2

mv nextcloud /var/www/html

rm -rf nextcloud nextcloud-${NEXTCLOUD_VERSION}.tar.bz2 nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.sha256 nextcloud-${NEXTCLOUD_VERSION}.tar.bz2.asc nextcloud.asc
